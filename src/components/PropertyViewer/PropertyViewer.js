import React from "react";

import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@mui/material";

const PropertyViewer = ({ pickedItem }) => {
  return (
    <Grid item xs={3} lg={3} sx={{ px: 2 }}>
      {pickedItem && (
        <Paper sx={{ width: "100%", mb: 2 }}>
          <TableContainer>
            <Table
              sx={{ minWidth: "100%", overflowY: "auto" }}
              aria-labelledby="tableTitle"
              size="medium"
            >
              <TableBody>
                <TableRow>
                  <TableCell component="th">Global Id</TableCell>
                  <TableCell>
                    {pickedItem?.GlobalId?.value
                      ? pickedItem.GlobalId.value
                      : ""}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th">Name</TableCell>
                  <TableCell>
                    {pickedItem?.Name?.value ? pickedItem.Name.value : ""}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th">Long Name</TableCell>
                  <TableCell>
                    {pickedItem?.LongName?.value
                      ? pickedItem.LongName.value
                      : ""}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th">Type</TableCell>
                  <TableCell>
                    {pickedItem?.CompositionType?.value
                      ? pickedItem.CompositionType.value
                      : ""}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      )}
    </Grid>
  );
};

export default PropertyViewer;
