import React from "react";

import { TreeItem } from "@mui/lab";
import { ExpandMore, ChevronRight } from "@mui/icons-material";

/*
   - return sidebar hierarchy UI.
   - generate tree skeleton of IFC file as JS object. 
     where,
      - each node is an express id
      - last node contain array of express id. 
  */
const structureTree = ({
  structure,
  treeSkeleton,
  parentId = null,
  heighlightIfcItem,
}) => {
  // if it has no children . then it wiil be the last node.
  const isLastNode = structure.children.length === 0;
  if (!isLastNode) {
    // initialising empty object for upcoming treeSkeleton reference.
    treeSkeleton[structure.expressID] = {};
  } else {
    // checking is initialized as array or not ?
    const isLastNodeObject = !Array.isArray(treeSkeleton[parentId]);
    if (isLastNodeObject) {
      // replacing emptyobject with new array for pushing upcoming child.
      treeSkeleton[parentId] = [];
      treeSkeleton[parentId].push(structure.expressID); // pushing first child
    } else {
      treeSkeleton[parentId].push(structure.expressID); // pushing all other childs
    }
  }

  let name = structure?.Name?.value;
  let longName = structure?.LongName?.value;
  let label = "";

  if (name && isNaN(name)) {
    label = name;
  } else if (name && !isNaN(name)) {
    label = `${name} ${longName ? "( " + longName + " )" : ""}`;
  } else {
    label = `${structure.type}`;
  }

  return (
    <TreeItem
      key={structure.expressID}
      nodeId={`${structure.expressID}`}
      label={label}
      sx={{ p1: 3, mb: 1, fontSize: "smaller", width: "100%" }}
      onClick={() =>
        heighlightIfcItem(
          structure.expressID,
          // passing reference of subTree to the highlight func or list of lastNode
          treeSkeleton[isLastNode ? parentId : structure.expressID]
        )
      }
      collapseIcon={<ExpandMore />}
      expandIcon={<ChevronRight />}
    >
      {structure?.children.map((item, index) =>
        structureTree({
          structure: item,
          treeSkeleton: treeSkeleton[structure.expressID],
          parentId: structure.expressID,
          heighlightIfcItem: heighlightIfcItem,
        })
      )}
    </TreeItem>
  );
};

export default structureTree;
