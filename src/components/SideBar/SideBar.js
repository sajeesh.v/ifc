import React from "react";

import { Grid } from "@mui/material";
import { TreeItem, TreeView } from "@mui/lab";
import structureTree from "./structureTree";

const SideBar = React.memo(
  ({ ifcStructure, treeSkeleton, heighlightIfcItem }) => {
    return (
      <Grid item xs={3} lg={3} sx={{ p: 3 }}>
        <TreeView
          aria-label="file system navigator"
          sx={{
            height: "100%",
            maxHeight: "100vh",
            flexGrow: 1,
            maxWidth: 400,
            overflowY: "auto",
          }}
        >
          {ifcStructure?.children?.length &&
            structureTree({
              structure: ifcStructure.children[0],
              treeSkeleton: treeSkeleton,
              parentId: null,
              heighlightIfcItem,
            })}
        </TreeView>
      </Grid>
    );
  }
);

export default SideBar;
