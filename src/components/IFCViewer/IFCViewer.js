import React from "react";
import { Grid } from "@mui/material";
import styled from "@emotion/styled";

const IFCViewer = React.forwardRef((props, ref) => {
  return (
    <StyledGrid ref={ref} item xs={6} lg={6}>
      <StyledIFCViewer></StyledIFCViewer>
    </StyledGrid>
  );
});

export default IFCViewer;

const StyledGrid = styled(Grid)`
  position: relative;

  & > article {
    padding: 1rem;
    background: red;

    position: absolute;
    z-index: 3;

    top: 0;
    left: 5rem;
  }
`;

const StyledIFCViewer = styled.div`
  position: relative;
  height: 100%;
  width: 50vw;
  background: #000;

  & > div {
    visibility: visible;
  }
`;
