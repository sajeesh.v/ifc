export const getLastNodeAsArray = (tree) => {
  let lastNodes = [];

  const traverse = (tree) => {
    if (Array.isArray(tree)) {
      // adding each subTree's last child.
      lastNodes = [...lastNodes, ...tree];

      return;
    } else {
      for (let key in tree) {
        traverse(tree[key]);
      }
    }
  };

  traverse(tree);

  return lastNodes;
};
