import React from "react";

const useEvents = () => {
  const _handleClick = (event) => {
    console.log("clicking...", event);
  };

  const handleEvent = (event, type) => {
    switch (type) {
      case "click":
        _handleClick(event);
        break;

      default:
        break;
    }
  };

  return { handleEvent };
};

export default useEvents;
