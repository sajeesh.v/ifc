import React from "react";
import { useSocketData } from "./useSocketData";
import useDisplayData from "./useDisplayData";
import useEvents from "./useEvents";

const useMiddleWares = ({ viewer, model }) => {
  // displaying UI children over Model
  const { appendDataToModel } = useDisplayData({ viewer, model });

  // socket connection
  useSocketData({ viewer, model, appendDataToModel });

  // handling user interaction
  const { handleEvent } = useEvents();

  return {};
};

export default useMiddleWares;
