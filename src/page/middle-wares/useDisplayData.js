import React, { useEffect, useState, useRef } from "react";

import ReactDOM from "react-dom";
import { MeshLambertMaterial } from "three";

import { CSS2DObject } from "../../css2dlib/CSS2DRenderer";
import styled from "styled-components";

const subset = {};
const childSetters = {};

const useDisplayData = ({ viewer, model }) => {
  const _getCenterVertex = (geometry) => {
    const coordinatesX = [];
    const coordinatesY = [];
    const coordinatesZ = [];
    const alreadySaved = new Set();
    const position = geometry.attributes.position;
    for (let index of geometry.index.array) {
      if (!alreadySaved.has(index)) {
        coordinatesX.push(position.getX(index));
        coordinatesY.push(position.getY(index));
        coordinatesZ.push(position.getZ(index));
        alreadySaved.add(index);
      }
    }

    coordinatesX.sort((x, y) => x - y);
    coordinatesY.sort((x, y) => x - y);
    coordinatesZ.sort((x, y) => x - y);

    const length = Math.floor(coordinatesX.length / 2);

    return {
      x: coordinatesX[length],
      y: coordinatesY[length],
      z: coordinatesZ[length],
    };
  };

  const _createSubset = (data, material) => {
    const scene = viewer?.context.getScene();

    subset[data.expressID] = viewer.IFC.loader.ifcManager.createSubset({
      modelID: model.modelID,
      ids: [data.expressID],
      material: material,
      scene: scene,
      removePrevious: true,
    });
    subset[data.expressID].geometry.computeBoundingSphere();

    const el = document.createElement("div");
    el.style.position = "relative";
    ReactDOM.render(
      <Content expressID={data.expressID} content={data.content} />,
      el
    );

    const obj = new CSS2DObject(el);
    const max = _getCenterVertex(subset[data.expressID].geometry);
    obj.position.set(max.x, max.y, max.z);
    subset[data.expressID].add(obj);
  };

  const _updateSubset = (data, material) => {
    subset[data.expressID].material = material;
    // setting corresponding child of subset.
    const setContent = childSetters[data.expressID];
    setContent((s) => ({ ...s, ...data.content }));
  };

  const appendDataToModel = (data, alert = false) => {
    const material = new MeshLambertMaterial({
      transparent: true,
      opacity: 0.6,
      color: alert ? 0xdc3545 : 0xfce700,
      depthTest: false,
    });

    const isSubSetCreated = subset[data.expressID] !== undefined;

    if (isSubSetCreated) {
      _updateSubset(data, material);
    } else {
      _createSubset(data, material);
    }
  };

  return {
    appendDataToModel,
  };
};

export default useDisplayData;

const Content = ({ content: contentProps, expressID }) => {
  const [isExpand, setIsExpand] = useState(false);
  const [content, setContent] = useState(contentProps);

  useEffect(() => {
    childSetters[expressID] = setContent;
  }, []);

  if (!isExpand)
    return (
      <StyledIndicator
        id={expressID}
        onClick={() => setIsExpand(true)}
        color={getColor(expressID)}
      />
    );

  return (
    <StyledContainer id={expressID} color={"whitesmoke"}>
      <>
        <span onClick={() => setIsExpand(false)}>x</span>
        <div>item: {content.item}</div>
        <div>status: {String(content.on_status)}</div>
        <div>frequent data: {String(content.frequent_data)}</div>
        <div>Repair Need: {String(content.repair_needed)}</div>
      </>
    </StyledContainer>
  );
};

const getColor = (id) => {
  let randomLessThan361 = Math.floor(id * 100) % 361;
  return `hsla(${randomLessThan361}, 80%, 60%, 1)`;
};

const StyledIndicator = styled.div`
  cursor: pointer;
  pointer-events: all !important;

  width: 2rem;
  height: 2rem;

  border-radius: 5rem;
  background-color: ${({ color }) => color};
`;

const StyledContainer = styled.div`
  /* max-width: 3rem; */
  position: relative;

  top: 1rem;
  left: 1rem;
  width: 9rem;
  /* height: 4rem; */
  font-size: 0.9rem;
  color: gray;
  padding: 0.3rem;
  pointer-events: all !important;
  background-color: ${({ color }) => color};

  & > span {
    display: block;
    width: 1rem;
    margin-left: auto;
    border-radius: 5rem;
    color: tomato;
    cursor: pointer;
  }
`;
