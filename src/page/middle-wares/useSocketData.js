import React from "react";
import { io } from "socket.io-client";

const socket = io("http://localhost:8000", { transports: ["websocket"] });

export const useSocketData = ({ viewer, model, appendDataToModel }) => {
  const alertHandle = () => {
    socket.off("risk-alert");
    socket.on("risk-alert", function (message) {
      if (message.data.length) {
        for (let item of message.data) {
          appendDataToModel(item, item.content.repair_needed);
        }
      }
    });
  };

  React.useEffect(() => {
    if (viewer && model) {
      alertHandle();
    }
  }, [viewer, model]);

  return {};
};
