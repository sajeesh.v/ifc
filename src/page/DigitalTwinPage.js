import React, { Fragment } from "react";
import { Grid } from "@mui/material";
import SideBar from "../components/SideBar/SideBar";
import PropertyViewer from "../components/PropertyViewer/PropertyViewer";
import IFCViewer from "../components/IFCViewer/IFCViewer";
import useDigitalTwinPage from "./useDigitalTwinPage.Logic";
import useMiddleWares from "./middle-wares";

const DigitalTwinPage = () => {
  const {
    viewer,
    model,
    pickedItem,
    heighlightIfcItem,
    ifcStructure,
    treeSkeletonRef,
    ifcViewerRef,
  } = useDigitalTwinPage();

  useMiddleWares({ viewer, model });

  return (
    <Wrapper>
      <SideBar
        heighlightIfcItem={heighlightIfcItem}
        ifcStructure={ifcStructure}
        treeSkeleton={treeSkeletonRef.current}
      />
      <IFCViewer ref={ifcViewerRef} viewer={viewer} model={model} />
      <PropertyViewer pickedItem={pickedItem} />
    </Wrapper>
  );
};

export default DigitalTwinPage;

const Wrapper = ({ children }) => (
  <Grid
    container
    spacing={0}
    sx={{
      background: "#000",
      color: "#fff",
      py: "10px",
    }}
  >
    {children}
  </Grid>
);
