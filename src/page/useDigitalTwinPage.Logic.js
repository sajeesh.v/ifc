import React, { useRef, useState, useEffect } from "react";
import { IfcViewerAPI } from "web-ifc-viewer";
import { getLastNodeAsArray } from "../utils/helper";

const useDigitalTwinPage = () => {
  const [viewer, setViewer] = useState(null);
  const [model, setModel] = useState(null);

  const [ifcStructure, setIfcStructure] = useState("");
  const [pickedItem, setPickedItem] = useState("");

  const ifcViewerRef = useRef(null);

  // holding the reference of uploading file as tree structure
  const treeSkeletonRef = useRef({});

  const ModelView = async () => {
    const container = ifcViewerRef.current.children[0];
    const viewerAPI = new IfcViewerAPI({ container });
    console.log("====viewerAPI====", viewerAPI);
    viewerAPI.axes.setAxes();
    viewerAPI.grid.setGrid(50, 10);
    viewerAPI.IFC.setWasmPath("wasm/");
    viewerAPI.IFC.applyWebIfcConfig({
      COORDINATE_TO_ORIGIN: true,
    });
    const model = await viewerAPI.IFC.loadIfcUrl(
      "http://localhost:8080/static/4.ifc",
      true
    );
    const structure = await viewerAPI.IFC.getSpatialStructure(
      model.modelID,
      true
    );
    setViewer(viewerAPI);
    setModel(model);
    if (structure) {
      setIfcStructure(structure);
    }
  };

  // const pickIfcItem = async () => {
  //   const { modelID, id } = await viewer.IFC.selector.pickIfcItem(true);
  //   const props = await viewer.IFC.getProperties(modelID, id, true, false);
  //   setPickedItem(props);
  // };

  const heighlightIfcItem = React.useCallback(
    async (elementid, data) => {
      /* 
     argument data can be array or object.
     - object : sub tree of tree skeleton
     - array : list of expressId of lastNodes itself
    */

      let ids = [];

      const isArrayOfLastNodes = Array.isArray(data);

      if (isArrayOfLastNodes) {
        ids = [elementid];
      } else {
        // generating whole lastNode's expressId of given subTree  as an Array
        ids = getLastNodeAsArray(data);
      }

      console.log("====ids=====", ids);

      /*
    - need expressIds of last nodes of the tree as an array
    - won't work if we pass expressId of node having children
    */
      await viewer.IFC.selector.pickIfcItemsByID(model.modelID, ids, true);
      await viewer
        .getProperties(model.modelID, elementid, true, false)
        .then((props) => {
          setPickedItem(props);
        });
    },
    [viewer, model]
  );

  useEffect(() => {
    ModelView();
  }, []);

  return {
    viewer,
    model,
    pickedItem,
    heighlightIfcItem,
    ifcStructure,
    treeSkeletonRef,
    ifcViewerRef,
  };
};

export default useDigitalTwinPage;
