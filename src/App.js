import React, { Fragment } from "react";
import styled from "styled-components";
import DigitalTwinPage from "./page/DigitalTwinPage";
import { HashRouter, Switch, Route } from "react-router-dom";

const App = () => {
  return (
    <Fragment>
      <HashRouter>
        <AppDiv>
          <Switch>
            <Route path="/" component={DigitalTwinPage} />
          </Switch>
        </AppDiv>
      </HashRouter>
    </Fragment>
  );
};

export default App;

export const AppDiv = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  height: 100vh;
  margin: 0rem;
`;
