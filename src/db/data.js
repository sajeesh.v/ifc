const jsonDataObj = [
  {
    expressID: 6627,
    content: {
      item: "WINDOW 1",
      on_status: true,
    },
  },
  {
    expressID: 18979,
    content: {
      item: "CHAIR 1",
      on_status: false,
    },
  },
  {
    expressID: 19159,
    content: {
      item: "TABLE 3",
      on_status: true,
    },
  },
];
const expressIDs = [6627, 18979, 19159];

export const updateData = () => {
  return setInterval(() => {
    const randomIndex = Math.floor(Math.random() * expressIDs.length);
    const tempData = jsonDataObj[randomIndex];
    jsonDataObj[randomIndex].content.on_status = !tempData.content.on_status;
  }, 3000);
};

export const getData = (expressID = 6627) => {
  return jsonDataObj.find((item) => item.expressID === expressID);
};

export const getAllData = () => jsonDataObj;

export const getOne = () => getData(6627);
