const express = require("express");
const app = express();
const http = require("http");
const cors = require("cors");
const socket = require("socket.io");
const {
  getAllData,
  updateData,
  getEmitData,
  getData,
  getOne,
} = require("./db/data");
app.use(express.json());
app.use(cors());
updateData();
app.get("/data", (req, res) => {
  const data = getAllData();
  return res.status(200).send(data);
});

const server = http.Server(app);
const io = socket(server);

const emitRiskAlert = (socket) => {
  const data = getAllData();
  if (data.length) {
    socket.emit("risk-alert", {
      data: data,
    });
  }
};

// ========socket connection ================
io.on("connection", function (socket) {
  emitRiskAlert(socket);
  setInterval(() => {
    emitRiskAlert(socket);
  }, 2000);
});

server.listen(8000, () => {
  console.log("socket server started");
});
