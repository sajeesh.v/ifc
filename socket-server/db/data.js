const jsonDataObj = [
  {
    expressID: 6627,
    content: {
      item: "WINDOW 1",
      on_status: true,
      repair_needed: false,
      frequent_data: Math.floor(Math.random() * 100000),
    },
  },
  {
    expressID: 18979,
    content: {
      item: "CHAIR 1",
      on_status: false,
      repair_needed: true,
      frequent_data: Math.floor(Math.random() * 100000),
    },
  },
  {
    expressID: 19159,
    content: {
      item: "TABLE 3",
      on_status: true,
      repair_needed: false,
      frequent_data: Math.floor(Math.random() * 100000),
    },
  },
];
const expressIDs = [6627, 18979, 19159];

const updateData = () => {
  return setInterval(() => {
    const randomIndex = Math.floor(Math.random() * expressIDs.length);
    const tempData = jsonDataObj[randomIndex];
    jsonDataObj[randomIndex].content.on_status = !tempData.content.on_status;
    jsonDataObj[randomIndex].content.repair_needed =
      !tempData.content.repair_needed;
    jsonDataObj[randomIndex].content.frequent_data = Math.floor(
      Math.random() * 100000
    );
  }, 500);
};

const getData = (expressID = 6627) => {
  return jsonDataObj.find((item) => item.expressID === expressID);
};

const getAllData = () => jsonDataObj;

const getOne = () => getData(6627);
// const getEmitData = () => {
//   const expressIDs = []
//   jsonDataObj.forEach(item => {
//     if (item.content.repair_needed) {
//       expressIDs.push(item)
//     }
//   })
//   return expressIDs
// }
module.exports = {
  getAllData,
  getData,
  updateData,
  getOne,
};
